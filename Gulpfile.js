var gulp = require("gulp");
var plumber = require("gulp-plumber");
var notify = require("gulp-notify");
var browserify = require("gulp-browserify");

var onError = function(err) {
  notify.onError({
    title:    "Gulp",
    subtitle: "Failure!",
    message:  "Error: <%= error.message %>",
    sound:    "Beep"
  })(err);

  this.emit('end');
};


gulp.task('browserify', function () {
    return gulp.src('./app/main.js')
        .pipe(plumber({errorHandler: onError}))
        .pipe(browserify())
        .pipe(gulp.dest('./.tmp/'));
});

gulp.task('watch', ['browserify'], function () {
    gulp.watch(['./app/**/*{.js,.jsx}'], ['browserify']);
});

