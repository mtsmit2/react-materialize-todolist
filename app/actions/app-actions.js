var appTree = require("../trees/app-tree");

var tree = appTree.tree;
var paths = appTree.paths;

module.exports = {
    addToList: function (itemText) {
        itemText = itemText || '';
        
        itemText = itemText.trim();
        
        if (itemText.length > 0) {
            var cursor = tree.select(paths.list);
            cursor.push(itemText);
        }
    }
};