var React = require("react");
var classSet = require("class-set");
var InputText = require("./input-text.jsx");
var appActions = require("../actions/app-actions");

var AddItem = React.createClass({
    change: function (e) {
    },
    addItem: function (e) {
        appActions.addToList(this.refs.itemInput.getValue());
        this.refs.itemInput.setFocus();
        this.refs.itemInput.clearValue();
    },
    render: function () {
        return (
            <div className="row">
                <InputText onChange={this.change} ref="itemInput" className="col s12" label="Add an item" />
                <a onClick={this.addItem} className="btn-floating btn-large red right"><i className="mdi-content-add"></i></a>
            </div>
        );
    }
});

module.exports = AddItem;