var React = require('react');

// Components
var Navbar = require('./navbar.jsx');
var TodoList = require("./todo-list.jsx");

var App = React.createClass({
    render: function () {
        return (
            <div className="">
                <Navbar />
                <div className="container">
                    <TodoList />
                </div>
            </div>
        );
    }
});

module.exports = App;