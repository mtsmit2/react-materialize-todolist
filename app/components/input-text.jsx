var React = require("react");
var classSet = require("class-set");
var Guid = require("guid");

var InputText = React.createClass({
    getInitialState: function () {
        return {
            active: false,
            value: '',
            guid: Guid.raw()
        };
    },
    focus: function (e) {
        this.setState({active: true});
        
        if (this.props.onFocus) { this.props.onFocus(e) }
    },
    blur: function (e) {
        if (this.state.value.length === 0) {
            this.setState({active: false});
        }
        
        if (this.props.onBlur) { this.props.onBlur(e) }
    },
    change: function (e) {
        this.setState({value: e.target.value});
        
        if (this.props.onChange) { this.props.onChange(e) }
    },
    getValue: function () {
       return this.isMounted() ? this.state.value : undefined;
    },
    clearValue: function () {
        this.setState({value: ''});
    },
    setFocus: function () {
        this.refs.textInput.getDOMNode().focus();
    },
    render: function () {
        var {
            label,
            icon:iconClass,
            props
        } = this.props;
        iconClass = iconClass || null;
        
        var iClasses = classSet({
            active: this.state.active,
            prefix: iconClass
        }, iconClass);
        
        var labelClasses = classSet({
            active: this.state.active
        });
        
        var icon = iconClass !== null ? <i className={iClasses}></i> : null;
        
        return (
            <div className="input-field col s12">
                {icon}
                <input ref="textInput" id={this.state.guid} type="text" onFocus={this.focus} onBlur={this.blur} onChange={this.change} value={this.state.value} />
                <label htmlFor={this.state.guid} className={labelClasses}>{label}</label>
            </div>
        );
    }
});

module.exports = InputText;
