var React = require("react");

var Navbar = React.createClass({
    
    render: function () {
        return (
            <nav>
            <div className="nav-wrapper green">
              <a href="#" className="brand-logo">React List</a>
            </div>
            </nav>
        );
    }
});

module.exports = Navbar;