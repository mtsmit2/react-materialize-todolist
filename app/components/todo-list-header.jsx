var React = require("react");

var TodoListHeader = React.createClass({
    render: function () {
        var {
            itemCount,
            ...props
        } = this.props;
        
        itemCount = itemCount || 0;
        var activeItems = itemCount === 0 ?
            'Add an item below' :
            'Active Items: ' + itemCount;
            
        return ( 
            <li className="collection-header">
                <h4>{activeItems}</h4>
            </li>
        );
    }
});

module.exports = TodoListHeader;