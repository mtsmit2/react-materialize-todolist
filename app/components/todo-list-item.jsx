var React = require("react");

var TodoListItem = React.createClass({
    render: function () {
        return (
            <li className="collection-item">
                <div>
                    {this.props.itemText}
                    <a href="#!" className="secondary-content">
                        <i className="mdi-content-clear"></i>
                    </a>
                </div>
            </li>
        );
    }
});

module.exports = TodoListItem;