var React = require("react");
var R = require("ramda");
var Guid = require("guid");

var TodoListHeader = require("./todo-list-header.jsx");
var TodoListItem = require("./todo-list-item.jsx");
var AddItem = require("./add-item.jsx");

var appTree = require("../trees/app-tree");

var TodoList = React.createClass({
    mixins: [appTree.tree.mixin],
    cursors: {
        list: appTree.paths.list
    },
    render: function () {
        var list = R.range(0,10);
        var listItems = R.mapIndexed(function (item, idx) {
            return (<TodoListItem key={idx} itemText={item} />);
        }, this.cursors.list.get());
        //<img src="https://letubeu.files.wordpress.com/2012/11/toms-daily-todo-list.jpg" />
        
        return (
            <div className="card">
                <div className="card-content">
                    <span className="card-title black-text">Your List</span>
                    <ul className="collection with-header">
                        <TodoListHeader itemCount={this.cursors.list.get().length} />
                        {listItems}
                    </ul>
                    <AddItem />
                </div>
            </div>
        );
    }
});

module.exports = TodoList;