var React = require("react");
var App = require("./components/app.jsx");

React.render(React.createFactory(App)(), document.querySelector('.app'));

