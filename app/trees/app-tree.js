var Baobab = require("baobab");

exports.paths = {
    list: ['list']
};

exports.tree = new Baobab(
    {
        list: []
    },
    {
        shiftReferences: true
    }
);