require('node-jsx').install({
    extension: '.jsx',
    harmony: 'true'
});

var express = require("express");
var path = require("path");
var fs = require("fs");
//var React = require("react");


var app = express();

var index = fs.readFileSync(path.resolve(__dirname, 'app/index.html')).toString();
// var AppComponent = React.createFactory(require("./app/components/app.jsx"));

app.use(express.static('.tmp'));
app.use('/material', express.static('./node_modules/materialize-css/dist'));


app.get('/', function (req, res) {
    var html = index.split('{{APP}}')
        // .join(React.renderToString(AppComponent()));
        .join('');
    res.send(html);
});

var server = app.listen(process.env.PORT, process.env.HOST, function () {

  var host = server.address().addres;
  var port = server.address().port;

  console.log('React List running at http://%s:%s', host, port);

});